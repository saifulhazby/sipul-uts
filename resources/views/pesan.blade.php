<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title> pesanna</title>
  </head>
  <body>
    <h1 class="text-center mb-4">LIST COSTUMER</h1>


    <div class="container">
        <a href="/tambahpesan" type="button" class="btn btn-primary">tambah pesanan +</a>
        <div class="row">
        @if ($message = Session::get('light'))
            <div class="alert alert-light" role="alert">
                {{ $message }}
            </div>
        @endif
            <table class="table table-dark table-striped">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">nama pembeli</th>
                    <th scope="col">jenis</th>
                    <th scope="col">nama pesanan</th>
                    <th scope="col">jumlah</th>
                    <th scope="col">tanggal</th>
                    <th scope="col">select</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($data as $row)
                <tr>
                    <th scope="row">{{ $row->id }}</th>
                    <td>{{ $row->namapembeli }}</td>
                    <td>{{ $row->jenis }}</td>
                    <td>{{ $row->namapesanan }}</td>
                    <td>{{ $row->jumlah }}</td>
                    <td>{{ $row->created_at->diffForHumans() }}</td>
                    <td>
                        <button type="button" class="btn btn-secondary">buang</button>
                        <a href="/tamdata/{{ $row->id }}"  class="btn btn-success">update</a>
                    </td>
                  </tr>
                  <tr>
                @endforeach
                </tbody>
              </table>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>