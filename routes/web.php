<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PeyeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('main');
});

Route::get('/pesan',[PeyeeController::class, 'index'])->name('pesan');

Route::get('/tambahpesan',[PeyeeController::class, 'tambahpesan'])->name('tambahpesan');
Route::post('/indata',[PeyeeController::class, 'indata'])->name('indata');

Route::get('/tamdata/{id}',[PeyeeController::class, 'tamdata'])->name('tamdata');
Route::post('/updata/{id}',[PeyeeController::class, 'updata'])->name('updata');