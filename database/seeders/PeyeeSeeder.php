<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PeyeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("peyees")->insert([
            'namapembeli' => 'safa',
            'jenis' => 'makanan',
            'namapesanan' => 'teote',
            'jumlah' => '5',
        ]);
    }
}
