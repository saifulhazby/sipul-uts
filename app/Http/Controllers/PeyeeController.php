<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peyee;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Stmt\Return_;

class PeyeeController extends Controller
{
    public function index(){
        $data = Peyee::all();
        return view('pesan',compact('data'));
    }

    public function tambahpesan(){
        return view('tambahpesan');
    }

    public function indata(Request $request){
       //dd($request->all());
        Peyee::create($request->all());
        return Redirect()->route('pesan')->with('light','data telah ditambahkan');
    }

    public function tamdata($id){
        $data = Peyee::find($id);
        //dd($data);
        return view('tamdata', compact('data'));
    }

    public function updata(Request $request, $id){
        $data = Peyee::find($id);
        $data->update($request->all());
        return Redirect()->route('pesan')->with('light','data berhasil di update');
    }
}
